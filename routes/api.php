<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/clients', 'API\V1\ClientController@index');
Route::get('/clients/{client}', 'API\V1\ClientController@show');
Route::post('/category', 'CategoryController@store');


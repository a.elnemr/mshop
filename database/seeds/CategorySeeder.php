<?php

use App\Category;

use Illuminate\Database\Seeder;

use Illuminate\Support\Str;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Category::class, 10)->create();
        factory(App\Category::class, 100)->create();
    }
}

@extends('layouts.app')

@section('title', 'All Products')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Products</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">admin </a></li>
              <li class="breadcrumb-item active">All products</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="card card-body">
              <div class="row">
                  <div class="col">
                        <form>
                        <select name="limit" id="">
                            <option value="5" {{ Request::get('limit') == 5? 'selected' : '' }}>5</option>
                            <option value="10" {{ Request::get('limit') == 10? 'selected' : '' }}>10</option>
                            <option value="25" {{ Request::get('limit') == 25? 'selected' : '' }}>25</option>
                        </select>
                        <button type="submit"> Update</button>
                    </form>
                  </div>

                  <div class="col text-center">
                  <form action="">
                        <input type="text" name="search" class="form-control">
                        <button type="submit" class="btn btn-info">Searching</button>
                         <a href="{{route('admin.products.index')}}">Reset</a>
                    </form>
                  </div>
                  <div class="col text-right">
                      @if (auth()->user()->is_Admin)
                          <a href="{{route('admin.products.create')}}" class="btn btn-success">create</a>

                      @endif
                  </div>
              </div>

          <table class="table table-bordered">
              <thead>
                  <tr>
                      <th>#</th>
                      <th>name</th>
                      <th>price</th>
                      <th>quantity</th>
                      <th>description</th>
                      <th>image</th>
                      <th>cat_ID</th>
                      <th>user_ID</th>
                      <th>Actions</th>
                  </tr>
              </thead>
              <tbody>
                  @foreach($products as $product)
                  <tr>
                      <td>{{ $product->id }}</td>
                      <td>{{ $product->name }}</td>
                      <td>{{ $product->price }}</td>
                      <td>{{ $product->quantity }}</td>
                      <td>{{ $product->description }}</td>
                      <td>{{ $product->image }}</td>
                      <td>{{ $product->category_id }}</td>
                      <td>{{ $product->user_id }}</td>

                      <td>

                          @can('products.show')

                            <a href="{{route('admin.products.show', $product)}}" class="btn btn-info">Show</a>

                          @endcan
                              @if (auth()->user()->is_Admin)

                              <a href="{{route('admin.products.edit', $product)}}" class="btn btn-primary">Edit</a>
                              @endif
                         <form action="{{ route('admin.products.destroy', $product) }}" method="post" class="d-inline-block">
                                      @csrf
                                      @method('DELETE')
                                      <button type="submit" class="btn btn-danger">Delete</button>
                          </form>
                      </td>
                  </tr>
                  @endforeach
              </tbody>
          </table>

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
@endsection

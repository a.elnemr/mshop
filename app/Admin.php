<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'users';

    protected static function booted()
    {
        static::addGlobalScope('is_admin', function (Builder $builder) {
            $builder->where('is_admin', 1);
        });
    }
}
